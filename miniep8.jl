function troca(v, i, j)
  aux = v[i]
  v[i] = v[j]
  v[j] = aux
end
  
function insercao(v)
  tam = length(v)
  for i in 2:tam
    j=i
    while j > 1
      if compareByValueAndSuit(v[j], v[j - 1])
        troca(v, j, j - 1)
      else
        break
      end
      j = j - 1
    end
  end
  return v
end

function compareByValue(x, y)
  cards = Dict("A" => 14, "K" => 13, "Q" => 12, "J" => 11, "10" => 10, "9" => 9, "8" => 8, "7" => 7, "6" => 6, "5" => 5, "4" => 4, "3" => 3, "2" => 2)
  value_x = cards[x[1:length(x)-1]]
  value_y = cards[y[1:length(y)-1]]
  if value_x < value_y
    return true
  else
    return false
  end
end

function compareByValueAndSuit(x, y)
  cards = Dict("A" => 14, "K" => 13, "Q" => 12, "J" => 11, "10" => 10, "9" => 9, "8" => 8, "7" => 7, "6" => 6, "5" => 5, "4" => 4, "3" => 3, "2" => 2)
  suits = Dict("♦" => 1, "♠" => 2, "♥" => 3, "♣" => 4) 
  suit_x = suits[x[length(x):length(x)]]
  suit_y = suits[y[length(y):length(y)]]
  value_x = cards[x[1:length(x)-1]]
  value_y = cards[y[1:length(y)-1]]
  if suit_x < suit_y
    return true
  elseif suit_x == suit_y
    if value_x < value_y
      return true
    else
      return false
    end
  else
    return false
  end
end


using Test
function teste1()
    println("Testando...")
    @test compareByValue("2♠", "A♠")
    @test !compareByValue("K♥", "10♥")
    @test !compareByValue("10♠", "10♥")
    @test !compareByValue("A♠", "2♥")
    @test compareByValue("9♠", "J♠")
    @test !compareByValue("Q♥", "Q♥")
    println("Tudo certo, parabéns humano!")
end

function teste2()
    println("Testando...")
    @test compareByValueAndSuit("2♠", "A♠")
    @test !compareByValueAndSuit("K♥", "10♥")
    @test compareByValueAndSuit("10♠", "10♥")
    @test !compareByValueAndSuit("8♥", "8♥")
    @test compareByValueAndSuit("A♠", "2♥")
    @test compareByValueAndSuit("9♠", "J♠")
    @test !compareByValueAndSuit("2♣", "A♠")
    @test !compareByValueAndSuit("Q♥", "Q♥")
    @test !compareByValueAndSuit("2♠", "10♦")
    println("Tudo certo, parabéns humano!")
end